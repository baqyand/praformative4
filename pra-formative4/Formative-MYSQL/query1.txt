1. membuat database formative 4
mysql>create database formative4;

2. membuat table student dan mengisi tipe data
mysql> create table formative4.student(
    -> id Int NOT NULL AUTO_INCREMENT,
    -> name VARCHAR(45) NULL,
    -> email VARCHAR(45) NULL,
    -> phone VARCHAR(45) NULL,
    -> birthdate DATE NULL,
    -> PRIMARY KEY (id));
Query OK, 0 rows affected (2.99 sec)

3. memastikan table terbuat di database
mysql> show tables;
+----------------------+
| Tables_in_formative4 |
+----------------------+
| student              |
+----------------------+

4. melihat isi columns dari table student
mysql> show columns from student;
+-----------+-------------+------+-----+---------+----------------+
| Field     | Type        | Null | Key | Default | Extra          |
+-----------+-------------+------+-----+---------+----------------+
| id        | int         | NO   | PRI | NULL    | auto_increment |
| name      | varchar(45) | YES  |     | NULL    |                |
| email     | varchar(45) | YES  |     | NULL    |                |
| phone     | varchar(45) | YES  |     | NULL    |                |
| birthdate | date        | YES  |     | NULL    |                |
+-----------+-------------+------+-----+---------+----------------+

5. membuat table course dan mengisi tipe data nya
mysql> create table formative3.course(
    -> id Int NOT NULL AUTO_INCREMENT,
    -> name VARCHAR(45) NULL,
    -> PRIMARY KEY(id));

6. memastikan table terbuat di database
mysql> show tables;
+----------------------+
| Tables_in_formative4 |
+----------------------+
| course               |
| student              |
+----------------------+

7. melihat columns course dan juga tipe datanya
mysql> show columns from course;
+-------+-------------+------+-----+---------+----------------+
| Field | Type        | Null | Key | Default | Extra          |
+-------+-------------+------+-----+---------+----------------+
| id    | int         | NO   | PRI | NULL    | auto_increment |
| name  | varchar(45) | YES  |     | NULL    |                |
+-------+-------------+------+-----+---------+----------------+

8, membuat table scoresheet dan menambahkan field / collumns
mysql> create table formative4.Scoresheet(
    -> id Int NOT NULL AUTO_INCREMENT,
    -> studentId Int,
    -> courseId Int,
    -> PRIMARY KEY (id));

9. memastikan table terdapat collumns
mysql> show columns from scoresheet;
+-----------+------+------+-----+---------+----------------+
| Field     | Type | Null | Key | Default | Extra          |
+-----------+------+------+-----+---------+----------------+
| id        | int  | NO   | PRI | NULL    | auto_increment |
| studentId | int  | YES  |     | NULL    |                |
| courseId  | int  | YES  |     | NULL    |                |
+-----------+------+------+-----+---------+----------------+

10. ALTER TABLE UNTUK MEMBUAT FORIGN KEY;

ALTER TABLE formative4.scoresheet ADD FOREIGN KEY (studentId) REFERENCES formative4.student(id);
ALTER TABLE formative4.scoresheet ADD FOREIGN KEY (courseId) REFERENCES formative4.course(id);

11. Menambahkan columns score pada table Scoresheet

 ALTER TABLE scoresheet ADD score VARCHAR(10) NULL;

12. Memastikan terjadi alter table pada table scoresheet

mysql>  show columns from scoresheet;
+-----------+-------------+------+-----+---------+----------------+
| Field     | Type        | Null | Key | Default | Extra          |
+-----------+-------------+------+-----+---------+----------------+
| id        | int         | NO   | PRI | NULL    | auto_increment |
| studentId | int         | YES  | MUL | NULL    |                |
| courseId  | int         | YES  | MUL | NULL    |                |
| score     | varchar(10) | YES  |     | NULL    |                |
+-----------+-------------+------+-----+---------+----------------+

13. mengisi data pada table student 5 data

a. student
mysql> INSERT INTO student(name,email,phone,birthdate) VALUES ('Bama', 'bamaqyand@gmail.com','08522395488','2003-02-11'),
    -> ('qyand','qyan@gmail.com', '0857002233','2002-02-12'),
    -> ('dija','dija@gmail.com', '0857089987','2003-03-13'),
    -> ('dean','dean@gmail.com', '08570022344','2005-01-14'),
    -> ('dra','dra@gmail.com', '08570233552','2004-04-15');

b. course
mysql> INSERT INTO formative4.course(name) VALUES ('MTK'),
    -> ('IND'),
    -> ('IPA'),
    -> ('BD'),
    -> ('PBO');

c. scoresheet
mysql> INSERT INTO formative4.scoresheet(studentId,courseId,score) VALUE('1','1','80'),
    -> ('1','2','85'),
    -> ('1','3','79'),
    -> ('1','4','78'),
    -> ('1','5','75'),
    -> ('2','1','82'),
    -> ('2','2','79'),
    -> ('2','3','77'),
    -> ('2','4','82'),
    -> ('2','5','85'),
    -> ('3','1','89'),
    -> ('3','2','82'),
    -> ('3','3','81'),
    -> ('3','4','70'),
    -> ('3','5','72'),
    -> ('4','1','78'),
    -> ('4','2','79'),
    -> ('4','2','77'),
    -> ('4','3','77'),
    -> ('4','4','79'),
    -> ('4','5','76'),
    -> ('5','1','82'),
    -> ('5','2','85'),
    -> ('5','3','85');

14. Show data semua table

a. student
mysql> select * from student;
+----+-------+---------------------+-------------+------------+
| id | name  | email               | phone       | birthdate  |
+----+-------+---------------------+-------------+------------+
|  1 | Bama  | bamaqyand@gmail.com | 08522395488 | 2003-02-11 |
|  2 | qyand | qyan@gmail.com      | 0857002233  | 2002-02-12 |
|  3 | dija  | dija@gmail.com      | 0857089987  | 2003-03-13 |
|  4 | dean  | dean@gmail.com      | 08570022344 | 2005-01-14 |
|  5 | dra   | dra@gmail.com       | 08570233552 | 2004-04-15 |
+----+-------+---------------------+-------------+------------+

b. course
mysql> select * from course;
+----+------+
| id | name |
+----+------+
|  1 | MTK  |
|  2 | IND  |
|  3 | IPA  |
|  4 | BD   |
|  5 | PBO  |
+----+------+

c. scoresheet

mysql> select * from scoresheet;
+----+-----------+----------+-------+
| id | studentId | courseId | score |
+----+-----------+----------+-------+
|  1 |         1 |        1 | 80    |
|  2 |         1 |        2 | 85    |
|  3 |         1 |        3 | 79    |
|  4 |         1 |        4 | 78    |
|  5 |         1 |        5 | 75    |
|  6 |         2 |        1 | 82    |
|  7 |         2 |        2 | 79    |
|  8 |         2 |        3 | 77    |
|  9 |         2 |        4 | 82    |
| 10 |         2 |        5 | 85    |
| 11 |         3 |        1 | 89    |
| 12 |         3 |        2 | 82    |
| 13 |         3 |        3 | 81    |
| 14 |         3 |        4 | 70    |
| 15 |         3 |        5 | 72    |
| 16 |         4 |        1 | 78    |
| 17 |         4 |        2 | 79    |
| 18 |         4 |        2 | 77    |
| 19 |         4 |        3 | 77    |
| 20 |         4 |        4 | 79    |
| 21 |         4 |        5 | 76    |
| 22 |         5 |        1 | 82    |
| 23 |         5 |        2 | 85    |
| 24 |         5 |        3 | 85    |
+----+-----------+----------+-------+
